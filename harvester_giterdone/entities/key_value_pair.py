from mongoengine import EmbeddedDocument, StringField

class KeyValuePair(EmbeddedDocument):
    key = StringField(max_length=120, required=True)
    value = StringField(max_length=120, required=True)
