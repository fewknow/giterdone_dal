from mongoengine import Document
from mongoengine import IntField


class ClientIds(Document):
    meta = {
        'collection': 'client_ids',
        'db_alias': 'giterdone',
        'indexes': [{'fields': ['seq'], 'unique': True}]
    }

    seq = IntField()