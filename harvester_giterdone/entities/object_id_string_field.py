from mongoengine.fields import ObjectIdField

class ObjectIdStringField(ObjectIdField):
    def to_mongo(self, value):
        mongoized = super(ObjectIdStringField, self).to_mongo(value)
        return str(mongoized)

    def to_python(self, value):
        pythonized = super(ObjectIdStringField, self).to_python(value)
        return str(pythonized)
