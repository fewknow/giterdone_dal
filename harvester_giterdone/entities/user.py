from harvester_giterdone.entities import BaseEntity
from mongoengine import BooleanField, EmbeddedDocument, EmbeddedDocumentField, ListField, StringField, IntField, DateTimeField, DecimalField
from datetime import datetime


class BagHistory(EmbeddedDocument):
    date = DateTimeField()
    offset = IntField()
    weight = DecimalField()
    status = StringField(max_length=32)


class Address(EmbeddedDocument):
    line_one = StringField(max_length=64)
    apartment = StringField(max_length=64)
    city = StringField(max_length=32)
    state = StringField(max_length=2)
    zip = IntField()


class Bag(EmbeddedDocument):
    label = StringField(max_length=32)
    bag_id = StringField(max_length=32)
    QR = StringField(max_length=120)
    status = StringField(max_length=120)
    history = ListField(EmbeddedDocumentField(BagHistory))


class Card(EmbeddedDocument):
    stripe_user_id = StringField(max_length=32)
    last_four_digits = IntField()
    type = StringField(max_length=16)
    cd = DateTimeField(default=datetime.now, required=True)


class User(BaseEntity):
    meta = {
        'collection': 'user',
        'db_alias': 'giterdone',
        'indexes': ['username']
    }

    username = StringField(max_length=120, required=True, unique=True)
    password = StringField(max_length=120)
    first_name = StringField(max_length=32)
    last_name = StringField(max_length=32)
    bags = ListField(EmbeddedDocumentField(Bag))
    client_id = IntField(required=True, unique=True)
    admin = BooleanField()
    address = EmbeddedDocumentField(Address)
    account_status = StringField(max_length=32, required=True)
    card = EmbeddedDocumentField(Card)
    password_reset_status = BooleanField()
