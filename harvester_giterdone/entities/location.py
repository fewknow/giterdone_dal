from harvester_giterdone.entities import BaseEntity
from mongoengine import EmbeddedDocumentField, EmbeddedDocument, StringField, IntField, GeoPointField


class Address(EmbeddedDocument):
	line_one = StringField(max_length=64, required=True)
	apartment = StringField(max_length=64, required=True)
	city = StringField(max_length=32, required=True)
	state = StringField(max_length=2, required=True)
	zip = IntField(required=True)


class Location(BaseEntity):
	meta = {
		'collection': 'location',
		'db_alias': 'giterdone',
		'indexes': []
	}

	name = StringField(max_length=120, required=True)
	business_name = StringField(max_length=120, required=True, unique=True)
	address = EmbeddedDocumentField(Address)
	coordinates = GeoPointField()
	phone = StringField(max_length=16, required=True)
