from datetime import datetime
from mongoengine import DateTimeField, Document, signals

def update_date(sender, document):
    document.update_date = datetime.now()

class BaseEntity(Document):
    meta = {
        'abstract': True
    }

    create_date = DateTimeField(db_field='cd', default=datetime.now, required=True)
    update_date = DateTimeField(db_field='ud', default=datetime.now, required=True)

signals.pre_save.connect(update_date)
