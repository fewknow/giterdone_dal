from harvester_giterdone.configuration import EnvConfiguration
from harvester_giterdone.configuration import YamlConfiguration
from harvester_giterdone.database import MongoEngineDatabase

def connect(configuration=None, file=None):
    if configuration is None:
        if file is None:
            configuration = EnvConfiguration()
        else:
            configuration = YamlConfiguration(file)
    MongoEngineDatabase(configuration).connect()
