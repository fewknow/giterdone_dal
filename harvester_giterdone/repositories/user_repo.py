from bson.objectid import ObjectId
from harvester_giterdone.repositories.read_only_clientids_repo import ReadOnlyClientIdsRepository
from harvester_giterdone.repositories.clientids_repo import ClientIdsRepository
from harvester_giterdone.entities import User as Entity
from harvester_giterdone.entities.user import Address
from harvester_giterdone.models import User as Model
from harvester_giterdone.entities.user import Bag
from harvester_giterdone.entities.user import Card
from harvester_giterdone.repositories import ReadOnlyUserRepository
from passlib.apps import custom_app_context as pwd_context

class UserRepository(ReadOnlyUserRepository):
    def create(self, **kwargs):
        kwargs['password'] = self._hash_password(kwargs['password'])
        kwargs['client_id'] = self.get_client_id()
        entity = Entity(**kwargs)
        entity.save()
        model_args = entity.to_mongo(use_db_field=False).to_dict()
        model_args['id'] = str(model_args['_id'])
        return Model(**model_args)

    def set_password(self, username, plain_text):
        hashed_password = self._hash_password(plain_text)
        query = {'username': username}
        update = {'set__password': hashed_password}
        Entity.objects(**query).update_one(**update)

    @staticmethod
    def _hash_password(password):
        password_hash = pwd_context.encrypt(password)
        return password_hash

    @staticmethod
    def update_user(**kwargs):
        query = {
            'username': kwargs["username"]
        }
        Entity.objects(**query).update_one(full_result=True, upsert=True, **kwargs)

    def reset_password(self, username, plain_text):
        self.set_password(username, plain_text)

    @staticmethod
    def get_client_id():
        read_repo = ReadOnlyClientIdsRepository()
        repo  = ClientIdsRepository()
        try:
            id = read_repo.find_max_id()
        except Exception as err:
            raise ValueError("Seq ID can not be incremented, err {e}".format(e=err))
        repo.create(seq=id)
        return id

    @staticmethod
    def add_bag_to_user(user_id, label):
        bag = Bag(label=label, status="empty")
        query = {'id': user_id}
        Entity.objects(**query).update_one(push__bags=bag)

    @staticmethod
    def add_card_to_user(user_id, **card):
        cd = Card(**card)
        query = {'id': user_id}
        Entity.objects(**query).update_one(set__card=cd)

    @staticmethod
    def add_address_to_user(user_id, **address):
        ad = Address(**address)
        query = {'id': user_id}
        Entity.objects(**query).update_one(set__address=ad)

    @staticmethod
    def update_account_status(status, user_id):
        query = {'id': user_id}
        Entity.objects(**query).update_one(set__account_status=status)

    @staticmethod
    def update_bag_status(status, user_id, label):
        Entity.objects(id=user_id, bags__label=label).update_one(set__bags__S__status=status)

    @staticmethod
    def update_bag_history(user_id, label, history):
        Entity.objects(id=user_id, bags__label=label).update_one(push__bags__S__history=history)

