from harvester_giterdone.entities import ClientIds as Entity

class ReadOnlyClientIdsRepository(object):
    def find_max_id(self):
        entity = Entity.objects.order_by("-seq").limit(-1).first()
        model_args = entity.to_mongo(use_db_field=False).to_dict()
        next_id = int(model_args['seq']) + 1
        return next_id