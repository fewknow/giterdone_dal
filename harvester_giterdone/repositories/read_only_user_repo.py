from harvester_giterdone.entities import User as Entity
from harvester_giterdone.models import User as Model
from passlib.apps import custom_app_context as pwd_context

class ReadOnlyUserRepository(object):
    def find_by_id(self, id):
        entity = Entity.objects.get(pk=id)
        model_args = entity.to_mongo(use_db_field=False).to_dict()
        model_args['id'] = str(model_args['_id'])
        return Model(**model_args)

    def find_by_username(self, username):
        entity = Entity.objects.get(username=username)
        model_args = entity.to_mongo(use_db_field=False).to_dict()
        model_args['id'] = str(model_args['_id'])
        return Model(**model_args)

    def find_by_client_id(self, client_id):
        entity = Entity.objects.get(client_id=client_id)
        model_args = entity.to_mongo(use_db_field=False).to_dict()
        model_args['id'] = str(model_args['_id'])
        return Model(**model_args)

    def find_admin_by_username(self, username):
        entity = Entity.objects.get(username=username,admin=True)
        model_args = entity.to_mongo(use_db_field=False).to_dict()
        model_args['id'] = str(model_args['_id'])
        return Model(**model_args)

    def verify_password(self, username, password):
        user = Entity.objects.get(username=username)
        return pwd_context.verify(password, user.password)

