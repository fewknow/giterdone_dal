from harvester_giterdone.entities import ClientIds as Entity


class ClientIdsRepository(object):

	def create(self, **kwargs):
		entity = Entity(**kwargs)
		entity.save()
		model_args = entity.to_mongo(use_db_field=False).to_dict()
		id = model_args['seq']
		return id
