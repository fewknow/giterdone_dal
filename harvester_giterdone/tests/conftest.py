from harvester_giterdone.configuration import EnvConfiguration
from harvester_giterdone.database import MongoEngineDatabase
import os
from pymongo import MongoClient

os.environ['GITERDONE_DATABASE_NAMES'] = 'giterdone'
os.environ['GITERDONE_DATABASE_GITERDONE_URL'] = 'mongodb://localhost:27017/giterdone_test'
os.environ['GITERDONE_DATABASE_GITERDONE_TEST_URL'] = 'mongodb://localhost:27017/giterdone_test'

def pytest_configure(config):
    print("Pytest Configuring")
    configuration = EnvConfiguration()
    mongoengine_database = MongoEngineDatabase(configuration)
    mongoengine_database.connect()

def pytest_unconfigure(config):
    print("Pytest UnConfiguring")
    mongo_client = MongoClient('localhost', 27017)
    mongo_client.drop_database('giterdone_test')
