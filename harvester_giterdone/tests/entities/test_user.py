from harvester_giterdone.entities import User
from harvester_giterdone.entities.user import Address, Bag, BagHistory
from unittest import TestCase
import datetime

class TestUser(TestCase):

    USERNAME = 'test user'
    PASSWORD = 'test password'
    ADMIN = True

    BAG_ID = "jsf-noke-01"
    CLIENT_ID = 100
    STATUS = 'email'
    LABEL = 'fewknow'
    QR='NA'


    line_one = 'line one address'
    line_two = 'line two address'
    city = 'test city'
    state = 'NA'
    zip = 24077
    password_reset_status = True


    @classmethod
    def setUpClass(self):
        self.user = User(
            bags=[
                Bag(
                    bag_id=self.BAG_ID,
                    label=self.LABEL,
                    QR=self.QR,
                    status=self.STATUS,
                    history=[
                        BagHistory(
                            date=datetime.datetime.utcnow(),
                            offset=-5,
                            weight=23.5,
                            status=self.STATUS
                        )
                    ]


                )
            ],
            client_id = self.CLIENT_ID,
            username=self.USERNAME,
            password=self.PASSWORD,
            admin=self.ADMIN,
            account_status=self.STATUS,
            address=Address(
                line_one=self.line_one,
                apartment=self.line_two,
                city=self.city,
                state=self.state,
                zip=self.zip
            ),
            password_reset_status=self.password_reset_status
        )

    def tearDown(self):
        User.drop_collection()

    def test_save(self):
        self.user.save()
