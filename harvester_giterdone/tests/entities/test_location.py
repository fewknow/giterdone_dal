from harvester_giterdone.entities import Location
from harvester_giterdone.entities.location import Address
from unittest import TestCase
import os

class TestUser(TestCase):
	NAME = 'test name'
	BUSINESS_NAME = 'test business'
	COORDINATES = [54.22, 23.54]
	PHONE = '(540)674-6453'

	LINE_ONE = 'line one address'
	LINE_TWO = 'line two address'
	CITY = 'test city'
	STATE = 'NA'
	ZIP = 24077

	@classmethod
	def setUpClass(self):
		self.location = Location(
			name = self.NAME,
			business_name = self.BUSINESS_NAME,
			coordinates = self.COORDINATES,
			phone = self.PHONE,
			address=Address(
				line_one=self.LINE_ONE,
				apartment=self.LINE_TWO,
				city=self.CITY,
				state=self.STATE,
				zip=self.ZIP
			)
		)

	def tearDown(self):
		Location.drop_collection()

	def test_save(self):
		self.location.save()
