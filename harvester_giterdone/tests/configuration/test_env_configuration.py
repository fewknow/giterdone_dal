from harvester_giterdone.configuration import EnvConfiguration
from harvester_giterdone.errors import ConfigurationError
import os
import pytest
from unittest import TestCase

class TestEnvConfiguration(TestCase):
    def setUp(self):
        os.environ['GITERDONE_DATABASE_NAMES'] = 'hello world'
        os.environ['GITERDONE_DATABASE_HELLO_URL'] = 'mongodb://localhost:27017/hello'
        os.environ['GITERDONE_DATABASE_WORLD_URL'] = 'mongodb://localhost:27017/world'
        self.configuration = EnvConfiguration()

    def test_create_configuration_requires_database_names_to_be_set(self):
        del os.environ['GITERDONE_DATABASE_NAMES']
        with self.assertRaises(ConfigurationError):
            configuration = EnvConfiguration()

    def test_create_configuration_requires_database_names_to_be_non_empty(self):
        os.environ['GITERDONE_DATABASE_NAMES'] = ''
        with self.assertRaises(ConfigurationError):
            configuration = EnvConfiguration()

    def test_create_configuration_requires_database_urls_to_be_set(self):
        del os.environ['GITERDONE_DATABASE_HELLO_URL']
        with self.assertRaises(ConfigurationError):
            configuration = EnvConfiguration()

    def test_create_configuration_requires_database_urls_to_be_non_empty(self):
        os.environ['GITERDONE_DATABASE_HELLO_URL'] = ''
        with self.assertRaises(ConfigurationError):
            configuration = EnvConfiguration()

    def test_keys_and_values(self):
        self.assertTrue('hello' in self.configuration)
        self.assertTrue('world' in self.configuration)
        self.assertEquals('mongodb://localhost:27017/hello',
                self.configuration['hello'])
        self.assertEquals('mongodb://localhost:27017/world',
                self.configuration['world'])
