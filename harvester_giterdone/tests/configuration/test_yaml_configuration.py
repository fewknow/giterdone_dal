from harvester_giterdone.configuration import YamlConfiguration
from harvester_giterdone.errors import ConfigurationError
import os
import pytest
import sys
from unittest import TestCase


class TestYamlConfiguration(TestCase):
	def setUp(self):
		testdir = os.path.dirname(__file__)
		print(testdir)
		self.configuration = YamlConfiguration(file=testdir+'/config.yml')
		print self.configuration


	def test_keys_and_values(self):
		self.assertEquals('mongodb://localhost:27017/hello',
						  self.configuration['hello'])
		self.assertEquals('mongodb://localhost:27017/world',
						  self.configuration['world'])
