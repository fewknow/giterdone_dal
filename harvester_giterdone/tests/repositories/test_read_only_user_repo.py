from harvester_giterdone.entities import User
from harvester_giterdone.entities.user import Address, Bag
from harvester_giterdone.repositories import ReadOnlyUserRepository
from passlib.apps import custom_app_context as pwd_context
from unittest import TestCase

class TestReadOnlyUserRepository(TestCase):

    ADDRESS_1 = Address(
        line_one='test line one 2',
        apartment='test line two 2',
        city='test city 2',
        state='VA',
        zip=24017

    )

    BAG_1 = Bag(
        bag_id="jsf-noke-01"
    )

    BAG_2 = Bag(
        bag_id="jsf-noke-02"
    )

    FIRST_1 = 'test first'
    FIRST_2 = 'test first two'
    LAST_1 = 'test last two'
    LAST_2 = 'test last'
    ADMIN_1 = False
    ADMIN_2 = True
    CLIENT_ID_1 = 1234
    CLIENT_ID_2 = 5678
    USERNAME_1 = 'maxenglander'
    USERNAME_2 = 'samflint'
    PASSWORD_1 = 'go for it, rock'
    PASSWORD_2 = 'casanova'
    STATUS = "email"
    password_reset_status = True

    def setUp(self):

        entity = User(
            first_name=self.FIRST_1,
            last_name=self.LAST_1,
            admin=self.ADMIN_1,
            client_id=self.CLIENT_ID_1,
            username=self.USERNAME_1,
            password=pwd_context.encrypt(self.PASSWORD_1),
            address=self.ADDRESS_1,
            bags=[self.BAG_1],
            account_status=self.STATUS,
            password_reset_status=self.password_reset_status
        )

        self.user_1 = entity.save()

        self.user_2 = User(
            first_name=self.FIRST_2,
            last_name=self.LAST_2,
            admin=self.ADMIN_2,
            client_id=self.CLIENT_ID_2,
            username=self.USERNAME_2,
            password=pwd_context.encrypt(self.PASSWORD_2),
            bags=[self.BAG_2],
            account_status=self.STATUS
            ).save()

        self.user_read_repo = ReadOnlyUserRepository()

    def tearDown(self):
        User.drop_collection()

    def test_find_by_client_id(self):
        user = self.user_read_repo.find_by_client_id(self.user_1.client_id)
        self.assertEquals(self.user_1.username, user.username)
        self.assertEquals(self.user_1.client_id, user.client_id)
        self.assertEquals(self.user_1.first_name, user.first_name)
        self.assertEquals(self.user_1.last_name, user.last_name)
        self.assertEquals(self.user_1.account_status, user.account_status)
        self.assertEquals(self.user_1.address['city'], user.address['city'])
        self.assertEquals(self.user_1.address['state'], user.address['state'])
        self.assertEquals(self.user_1.address['line_one'], user.address['line_one'])
        self.assertEquals(self.user_1.address['apartment'], user.address['apartment'])
        self.assertEquals(self.user_1.address['zip'], user.address['zip'])
        self.assertEquals(self.user_1.bags[0]['bag_id'], user.bags[0]['bag_id'])
        self.assertEquals(self.user_1.admin, user.admin)
        self.assertEquals(True, self.password_reset_status)
        self.assertEquals(True, pwd_context.verify(self.PASSWORD_1, user.password))

    def find_by_id(self):
        find_user = self.user_read_repo.find_by_client_id(self.user_1.client_id)
        user = self.user_read_repo.find_bu_id(self.user_1._id)
        self.assertEquals(self.user_1.username, user.username)
        self.assertEquals(self.user_1.client_id, user.client_id)
        self.assertEquals(self.user_1.first_name, user.first_name)
        self.assertEquals(self.user_1.last_name, user.last_name)
        self.assertEquals(self.user_1.account_status, user.account_status)
        self.assertEquals(self.user_1.address['city'], user.address['city'])
        self.assertEquals(self.user_1.address['state'], user.address['state'])
        self.assertEquals(self.user_1.address['line_one'], user.address['line_one'])
        self.assertEquals(self.user_1.address['apartment'], user.address['apartment'])
        self.assertEquals(self.user_1.address['zip'], user.address['zip'])
        self.assertEquals(self.user_1.bags[0]['bag_id'], user.bags[0]['bag_id'])
        self.assertEquals(self.user_1.admin, user.admin)
        self.assertEquals(True, self.password_reset_status)
        self.assertEquals(True, pwd_context.verify(self.PASSWORD_1, user.password))

    def test_find_by_name(self):
        user = self.user_read_repo.find_by_username(self.user_1.username)
        self.assertEquals(self.user_1.username, user.username)
        self.assertEquals(self.user_1.client_id, user.client_id)
        self.assertEquals(self.user_1.first_name, user.first_name)
        self.assertEquals(self.user_1.last_name, user.last_name)
        self.assertEquals(self.user_1.account_status, user.account_status)
        self.assertEquals(self.user_1.address['city'], user.address['city'])
        self.assertEquals(self.user_1.address['state'], user.address['state'])
        self.assertEquals(self.user_1.address['line_one'], user.address['line_one'])
        self.assertEquals(self.user_1.address['apartment'], user.address['apartment'])
        self.assertEquals(self.user_1.address['zip'], user.address['zip'])
        self.assertEquals(self.user_1.bags[0]['bag_id'], user.bags[0]['bag_id'])
        self.assertEquals(self.user_1.admin, user.admin)
        self.assertEquals(True, self.password_reset_status)
        self.assertEquals(True, pwd_context.verify(self.PASSWORD_1, user.password))


    def test_find_by_name_admin(self):
        user = self.user_read_repo.find_admin_by_username(self.user_2.username)
        self.assertEquals(self.user_2.username, user.username)
        self.assertEquals(self.user_2.client_id, user.client_id)
        self.assertEquals(self.user_2.first_name, user.first_name)
        self.assertEquals(self.user_2.last_name, user.last_name)

        self.assertEquals(self.user_2.bags[0]['bag_id'], user.bags[0]['bag_id'])
        self.assertEquals(self.user_2.admin, user.admin)
        self.assertEquals(True, pwd_context.verify(self.PASSWORD_2, user.password))
