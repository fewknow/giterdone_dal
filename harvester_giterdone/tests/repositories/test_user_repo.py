from bson import ObjectId
from harvester_giterdone.configuration import EnvConfiguration
from harvester_giterdone.database import MongoEngineDatabase
from harvester_giterdone.entities import User
from harvester_giterdone.entities.user import BagHistory
from harvester_giterdone.repositories import UserRepository
from harvester_giterdone.repositories.read_only_clientids_repo import ReadOnlyClientIdsRepository
from harvester_giterdone.repositories.clientids_repo import ClientIdsRepository
import unittest
import os
from datetime import datetime
import logging


class TestUserRepository(unittest.TestCase):
    PASSWORD = 'go for it rock'
    USERNAME = 'maxenglander_user'
    CLIENT_ID = -1

    CREATE_USER = "CREATE_USER"
    CREATE_PW = "CREATE_PW"
    ACCOUNT_STATUS = "email"

    def setUp(self):
        configuration = EnvConfiguration()
        mongoengine_database = MongoEngineDatabase(configuration)
        mongoengine_database.connect()
        try:
            read_client_repo = ReadOnlyClientIdsRepository()
            id = read_client_repo.find_max_id()
            client_repo = ClientIdsRepository()
            client_repo.create(seq=id)
        except Exception as e:
            client_repo = ClientIdsRepository()
            client_repo.create(seq=10000)

        self.repo = UserRepository()
        self.user = self.repo.create(username=self.USERNAME, password=self.PASSWORD, client_id=-1, account_status="email")

    def tearDown(self):
        User.drop_collection()

    def test_create(self):
        user = self.repo.create(
            username=self.CREATE_USER,
            password=self.CREATE_PW,
            account_status = self.ACCOUNT_STATUS
        )

        find_user = self.repo.find_by_client_id(user.client_id)
        self.assertNotEqual(None, find_user)
        self.assertNotEqual(self.CREATE_PW, find_user.password)
        self.assertEqual(self.CREATE_USER, find_user.username)
        self.assertEqual(user.client_id, find_user.client_id)
        self.assertEqual(user.account_status, find_user.account_status)

    def test_set_password(self):
        new_password = 'fightings all i know'
        self.repo.set_password(self.USERNAME, new_password)
        self.assertEquals(False, self.repo.verify_password(self.USERNAME, self.PASSWORD))
        self.assertEquals(True, self.repo.verify_password(self.USERNAME, new_password))

    def test_verify_password(self):
        user = self.user
        self.assertNotEqual(None, user)
        self.assertNotEqual(self.PASSWORD, user.password)
        self.assertEquals(True, self.repo.verify_password(self.USERNAME, self.PASSWORD))

    def test_add_bag_to_user(self):
        user = self.user
        self.repo.add_bag_to_user(user.id, "testing-bag")
        self.repo.add_bag_to_user(user.id, "testing-bag-01")
        self.repo.add_bag_to_user(user.id, "testing-bag-02")
        bag_user = self.repo.find_by_username(user.username)
        self.assertEqual(bag_user.bags[0]['label'], "testing-bag")
        self.assertEqual(bag_user.bags[1]['label'], "testing-bag-01")
        self.assertEqual(bag_user.bags[2]['label'], "testing-bag-02")
        self.assertEqual(bag_user.bags[0]['status'], "empty")

    def test_add_address_to_user(self):
        user = self.user
        address = {}
        address['line_one'] = "testing line1"
        address['apartment'] = "testing line2"
        address['city'] = "Roanoke"
        address['state'] = "VA"
        address['zip'] = 24077
        self.repo.add_address_to_user(user.id, **address)
        address_user = self.repo.find_by_username(user.username)
        self.assertEqual(address['line_one'], address_user.address['line_one'])
        self.assertEqual(address['apartment'], address_user.address['apartment'])
        self.assertEqual(address['city'], address_user.address['city'])
        self.assertEqual(address['state'], address_user.address['state'])
        self.assertEqual(address['zip'], address_user.address['zip'])

    def test_update_account_status(self):
        user = self.user
        self.repo.update_account_status(status='bag', user_id=user.id)
        status_user = self.repo.find_by_id(user.id)
        self.assertNotEqual(status_user.account_status, user.account_status)
        self.assertEqual(status_user.account_status, 'bag')

    def test_update_bag_status(self):
        self.repo.add_bag_to_user(user_id=self.user.id, label="testing-bag")
        user = self.repo.find_by_id(self.user.id)
        self.repo.update_bag_status(status='pickup', user_id=self.user.id, label=user.bags[0]['label'])
        user = self.repo.find_by_id(self.user.id)
        logging.warning(user)
        self.assertNotEqual(user.bags[0]['status'], 'empty')
        self.assertEqual(user.bags[0]['status'], 'pickup')

    def test_update_bag_history(self):
        history = BagHistory(
            date=datetime.utcnow(),
            offset=-5,
            weight=23.5,
            status="testing"
        )
        self.repo.add_bag_to_user(self.user.id, "testing-bag-history")
        self.repo.update_bag_history(history=history, user_id=self.user.id, label="testing-bag-history")
        user = self.repo.find_by_id(self.user.id)
        logging.warning(user.bags[0]['history'])
        self.assertIsNotNone(user.bags[0]['history'])
        self.assertEqual(user.bags[0]['history'][0]['offset'], history['offset'])
        self.assertEqual(user.bags[0]['history'][0]['weight'], history['weight'])
        self.assertEqual(user.bags[0]['history'][0]['status'], history['status'])

    def test_add_card_to_user(self):
        user = self.user
        card = {}
        card['stripe_user_id']="testing_stripe_user_id"
        card['last_four_digits']=4564
        card['type']="visa"
        card['cd']="2018-03-07T06:23:58.000Z"
        self.repo.add_card_to_user(user.id, **card)
        card_user = self.repo.find_by_username(user.username)
        self.assertEqual(card['stripe_user_id'], card_user.card['stripe_user_id'])
        self.assertEqual(card['last_four_digits'], card_user.card['last_four_digits'])
        self.assertEqual(card['type'], card_user.card['type'])
        self.assertEqual(datetime.strptime(card['cd'],"%Y-%m-%dT%H:%M:%S.%fZ"), card_user.card['cd'])

    def test_update_user_password_reset_status(self):
        user = self.user
        self.repo.update_user(password_reset_status=True, account_status=user.account_status, client_id=user.client_id, username=user.username)
        status_user = self.repo.find_by_id(user.id)
        self.assertNotEqual(status_user.password_reset_status, user.password_reset_status)
        self.assertEqual(status_user.password_reset_status, True)
