from harvester_giterdone.configuration import BaseConfiguration
from harvester_giterdone.errors import ConfigurationError
import os
import json
import yaml


class YamlConfiguration(BaseConfiguration):
    DATABASE_NAME_SEPARATOR = ' '
    DATABASE_NAMES = 'GITERDONE_DATABASE_NAMES'
    DATABASE_URL_PREFIX = 'GITERDONE_DATABASE'

    def __init__(self, file):
        super(YamlConfiguration, self).__init__()
        try:
            with open(file, 'r') as ymlfile:
                self.cfg = yaml.load(ymlfile)
        except Exception as e:
            raise ConfigurationError('Error opening file : {error}'.format(error=e))

        for db in self.cfg['mongo']['dbs']:
            print(self.cfg)
            print(db)
            try:
                key = 'mongodb://{host}:{port}/{database}'.format(host=self.cfg['mongo']['host'],
                                                                  port=self.cfg['mongo']['port'],
                                                                  database=db)
                self[db] = key
            except Exception as e:
                raise ConfigurationError('Error Building connection strings : {error}'.format(error=e))