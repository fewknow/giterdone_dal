from harvester_giterdone.configuration import IConfiguration
from harvester_giterdone.errors import ImmutableError


class BaseConfiguration(IConfiguration):

    @staticmethod
    def _raise_immutable_violation_error(self):
        raise ImmutableError('configuration may not be changed')

    __del_item = _raise_immutable_violation_error
    __set_item = _raise_immutable_violation_error
    clear = _raise_immutable_violation_error
    update = _raise_immutable_violation_error
    setdefault = _raise_immutable_violation_error
    popitem = _raise_immutable_violation_error
