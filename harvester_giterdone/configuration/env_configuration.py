from harvester_giterdone.configuration import BaseConfiguration
from harvester_giterdone.errors import ConfigurationError
import os


class EnvConfiguration(BaseConfiguration):
    DATABASE_NAME_SEPARATOR = ' '
    DATABASE_NAMES = 'GITERDONE_DATABASE_NAMES'
    DATABASE_URL_PREFIX = 'GITERDONE_DATABASE'

    def __init__(self):
        super(EnvConfiguration, self).__init__()

        if self.DATABASE_NAMES not in os.environ:
            raise ConfigurationError('ENV[{0}] not found'.format(self.DATABASE_NAMES))

        database_names_raw = os.environ[self.DATABASE_NAMES]
        if 0 is len(database_names_raw):
            raise ConfigurationError('ENV[{0}] is empty'.format(self.DATABASE_NAMES))

        database_names = database_names_raw.split(self.DATABASE_NAME_SEPARATOR)
        for database_name in database_names:
            key = '{0}_{1}_URL'.format(self.DATABASE_URL_PREFIX, database_name.upper())
            if key not in os.environ:
                raise ConfigurationError('ENV[{0}] not found'.format(key))
            if 0 is len(os.environ[key]):
                raise ConfigurationError('ENV[{0}] is empty'.format(key))
            self[database_name] = os.environ[key]


