from harvester_giterdone.errors import ReadOnlyError
from harvester_giterdone.errors import ValidationError

class BaseModel(object):
    def __init__(self, **kwargs):
        missing_fields = []

        for field in self.common_fields() + self.required_fields():
            if not field in kwargs:
                missing_fields.append(field)
            else:
                self.__dict__[field] = kwargs[field]

        if len(missing_fields):
            raise ValidationError('missing required fields [%s]' % ','.join(missing_fields))
        for field in self.optional_fields():
            self.__dict__[field] = kwargs.get(field, None)

    def to_dict(self):
        return self.__dict__.copy()

    @classmethod
    def fields(cls):
        return cls.optional_fields() + cls.required_fields()

    @classmethod
    def common_fields(cls):
        return [
            'create_date',
            'update_date'
        ]

    @classmethod
    def optional_fields(cls):
        raise NotImplementedError()

    @classmethod
    def required_fields(cls):
        raise NotImplementedError()

    @classmethod
    def _raise_read_only_error(cls):
        raise ReadOnlyError()

    def __delattr__(self, name):
        self._raise_read_only_error()

    def __setattr__(self, name, value):
        self._raise_read_only_error()
