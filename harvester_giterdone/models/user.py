from harvester_giterdone.models import BaseModel

class User(BaseModel):
    @classmethod
    def optional_fields(cls):
        return [
            'client_id',
            'first_name',
            'last_name',
            'address',
            'bags',
            'admin',
            'card',
            'password_reset_status'
        ]

    @classmethod
    def required_fields(cls):
        return [
            'id',
            'username',
            'password',
            'account_status'
        ]
