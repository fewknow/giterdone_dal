from harvester_giterdone.database import Database
from harvester_giterdone.configuration import IConfiguration
from mongoengine import register_connection

class MongoEngineDatabase(Database):
    def __init__(self, configuration):
        self.configuration = configuration

    def connect(self):
        for alias, url in self.configuration.items():
            register_connection(alias=alias, host=url)
