from setuptools import setup, find_packages

setup(
    # Application name:
    name="harvester_giterdone",

    # Version number (initial):
    version="0.0.6",

    # Application author details:
    author="Git Er Done LLC",
    author_email="contracts@giterdone.business",

    # Packages
    packages=find_packages(),
    platforms=["any"],

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="http://giterdone.io",
    # url="http://pypi.python.org/pypi/MyApplication_v010/",

    #
    # license="LICENSE.txt",
    description="A custom made DAL for Git Er Done.",

    # long_description=open("README.rst", "rt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "blinker==1.3",
        "pymongo==2.8",
        "mongoengine==0.15.0",
        "passlib==1.6.2",
        "PyYAML==3.11",
        "ddt==1.0.0"
    ],

    zip_safe=False
)
