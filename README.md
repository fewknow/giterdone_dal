#System Overview 

A custom DAL for Oden Technologies.

## Documentation

Install Sphinx:

    $ pip install -U sphinx

Setup Sphinx project:

    $ sphinx-quickstart \
        -q \
        --sep \
        --dot=. \
        -p harvester_oden \
        -a <author> \
        -v <version> \
        -r <release> \
        --ext-autodoc \
        --makefile \
        --batchfile \
        ./doc

Autogenerate Sphinx documents:

    $ sphinx-apidoc -o ./doc/source harvester_oden

Product HTML documentation:

    $ cd ./doc && make html
    
## Testing 

Install pytest: 

    $ pip install pytest
    $ pip install pytest-ordering

Run the tests: 

    $ py.test -v
