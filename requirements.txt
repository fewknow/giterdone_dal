PyYAML==3.11
blinker==1.3
ddt==1.0.0
pymongo==2.8
-e git+https://github.com/MongoEngine/mongoengine.git@42721628ebc48cf0a64084e31160b43c5b6853a2#egg=mongoengine-0.15.0
passlib==1.6.2
pytest==2.6.4
pytest-ordering==0.3
